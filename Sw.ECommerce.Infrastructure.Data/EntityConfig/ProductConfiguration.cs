﻿using Sw.ECommerce.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Sw.ECommerce.Infrastructure.Data.EntityConfig
{
    public class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            HasKey(p => p.ProductId);
            Property(p => p.Name).IsRequired().HasMaxLength(150);

        }
    }
}
