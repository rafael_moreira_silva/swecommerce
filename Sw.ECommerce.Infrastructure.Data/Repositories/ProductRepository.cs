﻿using System.Collections.Generic;
using System.Linq;
using Sw.ECommerce.Domain.Entities;
using Sw.ECommerce.Domain.Interfaces.Repositories;

namespace Sw.ECommerce.Infrastructure.Data.Repositories
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
      
        public IEnumerable<Product> FindPerName(string name)
        {
            return Db.Products.Where(x => x.Name == name);
        }
    }
}
