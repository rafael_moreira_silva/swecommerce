﻿using System.Collections.Generic;
using System.Linq;
using Sw.ECommerce.Domain.Entities;
using Sw.ECommerce.Domain.Interfaces.Repositories;

namespace Sw.ECommerce.Infrastructure.Data.Repositories
{
    public class PromotionRepository : RepositoryBase<Promotion>, IPromotionRepository
    {
  
        public IEnumerable<Promotion> FindPromotionActive(int productId)
        {
            return Db.Promotions.Where(p => p.Active && p.ProductId == productId);
        }
    }
}
