﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Sw.ECommerce.Application.Interfaces;
using Sw.ECommerce.Domain.Entities;
using Sw.ECommerce.MVC.ViewModels;

namespace Sw.ECommerce.MVC.Controllers
{
    public class SalesController : Controller
    {
        private readonly IProductAppService _productAppService;
        private readonly IPromotionAppSevice _promotionAppSevice;

        public SalesController(IProductAppService productAppService, IPromotionAppSevice promotionAppSevice)
        {
            _productAppService = productAppService;
            _promotionAppSevice = promotionAppSevice;

        }
        // GET: Sales
        public ActionResult Index()
        {
            List<SalesViewModel> salesViewModel = new List<SalesViewModel>(Mapper.Map<IEnumerable<Product>, IEnumerable<SalesViewModel>>(_productAppService.GetAll()));

                salesViewModel.AddRange(Mapper.Map<IEnumerable<Promotion>, IEnumerable<SalesViewModel>>(_promotionAppSevice.GetAll()));
            

            return View(salesViewModel);
        }

        // GET: Sales/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Sales/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sales/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Sales/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Sales/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Sales/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Sales/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
