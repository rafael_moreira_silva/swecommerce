﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using Sw.ECommerce.Application.Interfaces;
using Sw.ECommerce.Domain.Entities;
using Sw.ECommerce.MVC.ViewModels;

namespace Sw.ECommerce.MVC.Controllers
{
    public class PromotionController : Controller
    {

        private readonly IPromotionAppSevice _promotionAppSevice;
        private readonly IProductAppService _productAppService;

        public PromotionController(IPromotionAppSevice promotion, IProductAppService productAppService)
        {
            _promotionAppSevice = promotion;
            _productAppService = productAppService;

        }

        // GET: Promotion
        public ActionResult Index()
        {
            var promotionViewModel =Mapper.Map<IEnumerable<Promotion>, IEnumerable<PromotionViewModel>>(_promotionAppSevice.GetAll());
            return View(promotionViewModel);
        }

        // GET: Promotion/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Promotion/Create
        public ActionResult Create()
        {
            ViewBag.ProductId = new SelectList(_productAppService.GetAll(),"ProductId", "Name");
            return View();
        }

        // POST: Promotion/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(PromotionViewModel promotion)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var promotionDomain = Mapper.Map<PromotionViewModel, Promotion>(promotion);
                    _promotionAppSevice.AddPromotion(promotionDomain);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Promotion/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Promotion/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Promotion/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Promotion/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
