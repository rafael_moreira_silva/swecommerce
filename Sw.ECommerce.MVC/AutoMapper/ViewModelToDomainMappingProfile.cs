﻿using AutoMapper;
using Sw.ECommerce.Domain.Entities;
using Sw.ECommerce.MVC.ViewModels;

namespace Sw.ECommerce.MVC.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {

        public ViewModelToDomainMappingProfile()
        {
            CreateMap<ProductViewModel, Product>();
            CreateMap<PromotionViewModel, Promotion>();
            CreateMap<SalesViewModel, Promotion>();
            CreateMap<ProductViewModel, SalesViewModel>();
            CreateMap<PromotionViewModel, SalesViewModel>();
        }
    }
}

