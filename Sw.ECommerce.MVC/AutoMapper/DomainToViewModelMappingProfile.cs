﻿using AutoMapper;
using Sw.ECommerce.Domain.Entities;
using Sw.ECommerce.MVC.ViewModels;

namespace Sw.ECommerce.MVC.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Product, ProductViewModel>();
            CreateMap<Promotion, PromotionViewModel>();
            CreateMap<Product, SalesViewModel>();
            CreateMap<Promotion, SalesViewModel>();

        }

    }
}