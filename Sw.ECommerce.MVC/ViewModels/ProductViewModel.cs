﻿using System.ComponentModel.DataAnnotations;

namespace Sw.ECommerce.MVC.ViewModels
{
    public class ProductViewModel
    {
        [Key]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Preencha o campo Nome do Produto")]
        [MaxLength(150, ErrorMessage = "Máximo {0} caracteres")]
        [MinLength(2,ErrorMessage = "Mínimo {0} caracteres")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Preencha o campo Valor do Produto")]
        [DataType(DataType.Currency)]
        [Range(typeof(decimal),"0","9999999999")]
        public decimal Value { get; set; }
    }
}