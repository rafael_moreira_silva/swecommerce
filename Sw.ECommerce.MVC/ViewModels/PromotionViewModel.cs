﻿using Sw.ECommerce.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace Sw.ECommerce.MVC.ViewModels
{
    public class PromotionViewModel
    {
        [Key]
        public int PromotionId { get; set; }

        [Required(ErrorMessage = "Preencha o campo descrição da Promoção")]
        [MaxLength(150, ErrorMessage = "Máximo {0} caracteres")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Preencha o campo Desconto")]
        public decimal Discount { get; set; }

        [Required(ErrorMessage = "Preencha o campo Número de Itens para ativar a promoção")]
        public int NumberItemsForApplicability { get; set; }

        [Required(ErrorMessage = "Preencha o campo PromoçãoAtiva")]
        public bool Active { get; set; }

        [Required(ErrorMessage = "Preencha o campo Produto")]
        public int ProductId { get; set; }

        public virtual Product Product { get; set; }

         
    }
}