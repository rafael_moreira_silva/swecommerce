﻿namespace Sw.ECommerce.MVC.ViewModels
{
    public class SalesViewModel
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }

    }
}