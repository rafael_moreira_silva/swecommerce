﻿using System.Collections.Generic;
using System.Linq;
using Sw.ECommerce.Domain.Entities;
using Sw.ECommerce.Domain.Interfaces.Repositories;
using Sw.ECommerce.Domain.Interfaces.Services;

namespace Sw.ECommerce.Domain.Services
{
    public class PromotionService : ServiceBase<Promotion>, IPromotionService
    {
        private readonly IPromotionRepository _promotionRepository;

        public PromotionService(IPromotionRepository promotionRepository) : base(promotionRepository)
        {
            _promotionRepository = promotionRepository;
        }


        public IEnumerable<Promotion> FindPromotionActive(int productId)
        {
            return _promotionRepository.FindPromotionActive(productId);
        }

        public void AddPromotion(Promotion promotion)
        {
            if(!FindPromotionActive(promotion.ProductId).Any())
                _promotionRepository.Add(promotion);    
        }





    }
}
