﻿using System;
using System.Collections.Generic;
using Sw.ECommerce.Domain.Entities;
using Sw.ECommerce.Domain.Interfaces.Repositories;
using Sw.ECommerce.Domain.Interfaces.Services;

namespace Sw.ECommerce.Domain.Services
{
    public class ProductService : ServiceBase<Product>, IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository) : base(productRepository)
        {
            _productRepository = productRepository;
        }

        public IEnumerable<Product> FindPerName(string name)
        {
            return _productRepository.FindPerName(name);
        }
    }
}
