﻿using System.Collections.Generic;
using Sw.ECommerce.Domain.Entities;

namespace Sw.ECommerce.Domain.Interfaces.Repositories
{
    public interface IPromotionRepository : IRepositoryBase<Promotion>
    {
        IEnumerable<Promotion>  FindPromotionActive(int productId);
    }
}