﻿using System.Collections.Generic;
using Sw.ECommerce.Domain.Entities;

namespace Sw.ECommerce.Domain.Interfaces.Repositories
{
    public interface IProductRepository : IRepositoryBase<Product>
    {
        IEnumerable<Product> FindPerName(string name);
    }
}
