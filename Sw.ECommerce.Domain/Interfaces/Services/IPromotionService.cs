﻿using System.Collections.Generic;
using Sw.ECommerce.Domain.Entities;

namespace Sw.ECommerce.Domain.Interfaces.Services
{
    public interface IPromotionService : IServiceBase<Promotion>
    {
        void AddPromotion(Promotion promotion);
        IEnumerable<Promotion> FindPromotionActive(int idProduct);


    }
}
