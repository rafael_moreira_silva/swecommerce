﻿using System.Collections;
using System.Collections.Generic;
using Sw.ECommerce.Domain.Entities;

namespace Sw.ECommerce.Domain.Interfaces.Services
{
    public interface IProductService : IServiceBase<Product>
    {
        IEnumerable<Product> FindPerName(string name);
       

    }
}