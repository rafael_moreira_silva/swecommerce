﻿namespace Sw.ECommerce.Domain.Entities
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
}
