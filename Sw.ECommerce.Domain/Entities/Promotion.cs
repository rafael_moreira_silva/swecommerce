﻿namespace Sw.ECommerce.Domain.Entities
{
    public class Promotion
    {
        public int PromotionId { get; set; }
        public string Description { get; set; }
        public decimal Discount { get; set; }
        public int NumberItemsForApplicability { get; set; }
        public bool Active { get; set; }
        public int ProductId { get; set; }

        public virtual Product Product { get; set; }
    }

}
