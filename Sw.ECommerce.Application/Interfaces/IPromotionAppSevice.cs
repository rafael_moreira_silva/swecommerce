﻿using System.Collections.Generic;
using Sw.ECommerce.Domain.Entities;

namespace Sw.ECommerce.Application.Interfaces
{
    public interface IPromotionAppSevice : IAppServiceBase<Promotion>
    {
        IEnumerable<Promotion> FindPromotionActive(int idProduct);
        void AddPromotion(Promotion promotion);
    }
}