﻿using System.Collections.Generic;
using Sw.ECommerce.Domain.Entities;

namespace Sw.ECommerce.Application.Interfaces
{
    public interface IProductAppService : IAppServiceBase<Product>
    {
        IEnumerable<Product> FindPerName(string name);
    }
}
