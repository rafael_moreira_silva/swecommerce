﻿using System.Collections.Generic;
using Sw.ECommerce.Application.Interfaces;
using Sw.ECommerce.Domain.Entities;
using Sw.ECommerce.Domain.Interfaces.Services;

namespace Sw.ECommerce.Application
{
    public class ProductAppService : AppServiceBase<Product>, IProductAppService
    {
        private readonly IProductService _productService;

        public ProductAppService(IProductService productService) : base(productService)
        {
            _productService = productService;
        }

        public IEnumerable<Product> FindPerName(string name)
        {
            return _productService.FindPerName(name);
        }
    }
}
