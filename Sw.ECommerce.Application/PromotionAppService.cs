﻿using System.Collections.Generic;   
using Sw.ECommerce.Application.Interfaces;
using Sw.ECommerce.Domain.Entities;
using Sw.ECommerce.Domain.Interfaces.Services;

namespace Sw.ECommerce.Application
{
    public class PromotionAppService : AppServiceBase<Promotion>, IPromotionAppSevice
    {
        private readonly IPromotionService _promotionService;

        public PromotionAppService(IPromotionService promotionService) : base (promotionService)
        {
            _promotionService = promotionService;
        }

        public IEnumerable<Promotion> FindPromotionActive(int idProduct)
        {
            return _promotionService.FindPromotionActive(idProduct);
        }

        public void AddPromotion(Promotion promotion)
        {
            _promotionService.AddPromotion(promotion);
        }
    }
}
